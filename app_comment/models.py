from django.db import models


# Create your models here.


class Comment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=True, default='')
    comment_text = models.TextField(blank=True, null=True)
    author = models.ForeignKey('auth.User', related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return self.author.get_username()

    class Meta:
        ordering = ('created',)
