from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Comment


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Comment
        fields = ('url',  'id', 'created', 'title', 'comment_text', 'author')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    comments = serializers.HyperlinkedRelatedField(many=True, view_name='comment-detail', read_only=True, lookup_field='User.USERNAME_FIELD'),

    class Meta:
        model = User
        fields = ('url', 'id', 'username',  'comments')
