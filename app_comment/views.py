from django.contrib.auth.models import User
from .models import Comment
# from .permissions import IsOwnerOrReadOnly
from .serializers import CommentSerializer, UserSerializer
from rest_framework import (
    authentication,
    filters,
    permissions,
    viewsets
)


class DefaultMixin(object):
    """Default settings for view authentication, permissions,
    filtering and pagination."""

    authentication_classes = (
        authentication.BasicAuthentication,  # for Browsable API
        authentication.TokenAuthentication,  # for CURL based requests
    )

    permission_classes = (
        permissions.IsAuthenticated,
    )

    filter_backends = (
        filters.DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )

# @api_view(['GET'])
# def api_root(request, format=None):
#     return Response({
#         'users': reverse('user-list', request=request, format=format),
#         'comments': reverse('comment-list', request=request, format=format)
#     })


class CommentViewSet(DefaultMixin, viewsets.ModelViewSet):
    # """
    #     This viewset automatically provides `list`, `create`, `retrieve`,
    # `update` and `destroy` actions.

    # .
    # """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                       IsOwnerOrReadOnly,)
    search_fields = ('author__username',)
    ordering_fields = ('author', 'created',)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    #prevent pagination error


class UserViewSet(DefaultMixin, viewsets.ReadOnlyModelViewSet):
    # """
    # This viewset automatically provides `list` and `detail` actions.
    # """
    #queryset = User.objects.all()
    
    
    queryset = User.objects.order_by(User.USERNAME_FIELD)
    serializer_class = UserSerializer
    search_fields = (User.USERNAME_FIELD,)

    #prevent pagination error
