(function ($, Backbone, _, app) {

    var TemplateView = Backbone.View.extend({
        templateName: '',
        initialize: function () {
            this.template = _.template($(this.templateName).html());
        },
        render: function () {
            var context = this.getContext(),
            html = this.template(context);
            this.$el.html(html);
        },
        getContext: function () {
            return {};
        }
    });

    var FormView = TemplateView.extend({
        events: {
            'submit form' : 'submit' ,
            'click button.cancel' : 'done'
        },
        errorTemplate: _.template('<span class="error"><%- msg %></span>'),
        clearErrors: function () {
            $('.error', this.form).remove();
        },
        showErrors: function (errors) {
            _.map(errors, function (fieldErrors, name) {
                var field = $(':input[name=' + name + ']', this.form),
                    label = $('label[for=' + field.attr('id') + ']', this.form);
                if (label.length === 0) {
                    label = $('label', this.form).first();
                }
                function appendError(msg) {
                    label.before(this.errorTemplate({msg: msg}));
                }
                _.map(fieldErrors, appendError, this);
            }, this);
        },
        serializeForm: function (form) {
            return _.object(_.map(form.serializeArray(), function (item) {
                // Convert object to tuple of (name, value)
                return [item.name, item.value];
            }));
        },
        submit: function (event) {
            event.preventDefault();
            this.form = $(event.currentTarget);
            this.clearErrors();
        },
        failure: function (xhr, status, error) {
            var errors = xhr.responseJSON;
            this.showErrors(errors);
        },
        done: function (event) {
            if (event) {
                event.preventDefault();
            }
            this.trigger('done');
            this.remove();
        },
        modelFailure: function (model, xhr, options) {
            var errors = xhr.responseJSON;
            this.showErrors(errors);
        },
    });





    var LoginView = FormView.extend({
        id: 'login',
        templateName: '#login-template',
        submit: function (event) {
            var data = {};
            FormView.prototype.submit.apply(this, arguments);
            data = this.serializeForm(this.form);
            $.post(app.apiLogin, data)
                .done($.proxy(this.loginSuccess, this))
                .fail($.proxy(this.failure, this));
        },
        loginSuccess: function (data) {
            app.session.save(data.token);
            this.done();
        }
    });

    var HeaderView = TemplateView.extend({
        tagName: 'header',
        templateName: '#header-template',
        events: {
            'click a.logout': 'logout'
        },
        getContext: function () {
            return {authenticated: app.session.authenticated()};
        },
        logout: function (event) {
            event.preventDefault();
            app.session.delete();
            window.location = '/';
        }
    });

    var NewCommentView = FormView.extend({
        templateName: '#new-comment-template' ,
        className: 'new-comment' ,
        events: _.extend({
            'click button.cancel' : 'done' ,
        }, FormView.prototype.events),
        submit: function (event) {
            var self = this,
            attributes = {};
            FormView.prototype.submit.apply(this, arguments);
            attributes = this.serializeForm(this.form);
                app.collections.ready.done(function () {
                app.comments.create(attributes, {
                    wait: true,
                    success: $.proxy(self.success, self),
                    error: $.proxy(self.modelFailure, self)
                });
            });
        },
        success: function (model) {
            this.done();
            window.location.hash = '#comment/' + model.get('id' );
        }
    });

     // рендерим индивидуальный comment-элемент списка (li)
    var CommentView = FormView.extend({
        tagName: 'li',
        template: _.template($('#item-template').html()),
        render: function(){
          this.$el.html(this.template(this.model.toJSON()));
          this.input = this.$('.edit');
          return this; // включить цепочку вызовов
        },
        initialize: function(){
          this.model.on('change', this.render, this);
          this.model.on('destroy', this.remove, this);
        },
        events: {
          'dblclick label' : 'edit',
          'keypress .edit' : 'updateOnEnter',
          'blur .edit' : 'close',
          'click .toggle': 'toggleCompleted',
          'click .destroy': 'destroy'
        },
        edit: function(){
          this.$el.addClass('editing');
          this.input.focus();
        },
        close: function(){
          var value = this.input.val().trim();
          if(value) {
             this.model.save({title: value});
          }
          this.$el.removeClass('editing');
        },
        updateOnEnter: function(e){
          if(e.which == 13){
             this.close();
          }
        },
        toggleCompleted: function(){ //ctrem
                this.model.toggle();
        },
        destroy: function(){
              this.model.destroy();
        }
    });


    var HomepageView = Backbone.View.extend({
                templateName: '#home-template',
        events: {
            'click button.add' : 'renderAddForm'
        },
        initialize: function(options) {
            var self = this;
            TemplateView.prototype.initialize.apply(this, arguments);
            app.collections.ready.done(function () {
                var end = new Date();
                end.setDate(end.getDate() - 7);
                end = end.toISOString().replace(/T.*/g, '' );
                app.comments.fetch({
                    data: {end_min: end},
                    success: $.proxy(self.render, self)
                });
            });
        },
        getContext:function() {
            return {comments: app.comments || null};
        },
        renderAddForm: function (event) {
        var view = new NewCommentView(),
            link = $(event.currentTarget);
            event.preventDefault();
            link.before(view.el);
            link.hide();
            view.render();
            view.on('done' , function () {
                link.show();
            });
        }
       
      });






        







    app.views.HomepageView = HomepageView;
    app.views.LoginView = LoginView;
    app.views.HeaderView = HeaderView;
    app.views.CommentView = HeaderView;




})(jQuery, Backbone, _, app);