(function ($, Backbone, _, app) {

/*
    CSRF helper functions token directly from Django docs
*/

    function csrfSafeMethod(method) {
        // HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/i.test(method));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = $.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(
                        cookie.substring(name.length + 1)
                    );
                    break;
                }
            }
        }
        return cookieValue;
    }

/*
    Setup jQuery ajax calls to handle CSRF
*/

    $.ajaxPrefilter(function (settings, originalOptions, xhr) {
        var csrfToken;
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            csrfToken = getCookie('csrfToken');
            xhr.setRequestHeader('X-CSRFToken', csrfToken);
        }
    });

    var Session = Backbone.Model.extend({
        defaults: {
            token: null
        },
        initialize: function (options) {
            this.options = options;
            $.ajaxPrefilter($.proxy(this._setupAuth, this));
            this.load();
        },
        load: function () {
            var token = localStorage.apiToken;
            if (token) {
                this.set('token', token);
            }
        },
        save: function (token) {
            this.set('token', token);
            if (token === null) {
                localStorage.removeItem('apiToken');
            } else {
                localStorage.apiToken = token;
            }
        },
        delete: function () {
            this.save(null);
        },
        authenticated: function () {
            return this.get('token') != null;
        },
        _setupAuth: function (settings, originalOptions, xhr) {
            if (this.authenticated()) {
                xhr.setRequestHeader(
                    'Authorization',
                    'Token ' + this.get('token')
                );
            }
        }
    });

    app.session = new Session();

    app.models.Comment = Backbone.Model.extend({});
/*    app.models.User = Backbone.Model.extend({});  */
    app.models.User = Backbone.Model.extend({
        idAttributemodel: 'username'
    });

    var BaseCollection = Backbone.Collection.extend({
        parse: function (response) {
            this._next = response.next;
            this._previous = response.previous;
            this._count = response.count;
            return response.results || [];
        }
    });
    
    app.collections.ready = $.getJSON(app.apiRoot);
    
    app.collections.ready.done(function (data) {
        app.collections.Comments = BaseCollection.extend({
            model: app.models.Comment,
            url: data.comments
        });
        app.comments = new app.collections.Comments();
        app.collections.Users = BaseCollection.extend({
            model: app.models.User,
            url: data.users
        });
        app.users = new app.collections.Users();
    });

})(jQuery, Backbone, _, app);