from . import views
from rest_framework.routers import DefaultRouter

#getting view from coreapi package to include an API schema
#schema_view = get_schema_view(title='app_comment')

router = DefaultRouter(trailing_slash=False)
router.register(r'comments', views.CommentViewSet)
router.register(r'users', views.UserViewSet)
